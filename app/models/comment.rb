class Comment < ActiveRecord::Base
	validates_presence_of :post_id, only_integer:true
	validates_presence_of :body
	belongs_to :post
end
